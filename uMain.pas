unit uMain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, ComCtrls, ShellCtrls, Buttons, StdCtrls, FileCtrl,
  ExtCtrls, Jpeg, IniFiles, TGA, ShellAPI, ToolWin;

type
  TFormMain = class(TForm)
    MainMenu1: TMainMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    FileListBox1: TFileListBox;
    FilterComboBox1: TFilterComboBox;
    Edit1: TEdit;
    N4: TMenuItem;
    N5: TMenuItem;
    N6: TMenuItem;
    PopupMenu1: TPopupMenu;
    MenuItem1: TMenuItem;
    MenuItem2: TMenuItem;
    N10: TMenuItem;
    N13: TMenuItem;
    N11: TMenuItem;
    N12: TMenuItem;
    MenuItem3: TMenuItem;
    ShellTreeView1: TShellTreeView;
    ShellComboBox1: TShellComboBox;
    StatusBar1: TStatusBar;
    Panel1: TPanel;
    Image1: TImage;
    N501: TMenuItem;
    N251: TMenuItem;
    N2001: TMenuItem;
    N4001: TMenuItem;
    N4002: TMenuItem;
    N8001: TMenuItem;
    N252: TMenuItem;
    N502: TMenuItem;
    N2002: TMenuItem;
    N4003: TMenuItem;
    N8002: TMenuItem;
    N7: TMenuItem;
    N8: TMenuItem;
    N9: TMenuItem;
    N14: TMenuItem;
    N15: TMenuItem;
    N16: TMenuItem;
    N17: TMenuItem;
    N18: TMenuItem;
    TimerShow: TTimer;
    N19: TMenuItem;
    N20: TMenuItem;
    N21: TMenuItem;
    JPEG1: TMenuItem;
    N22: TMenuItem;
    SaveDialog1: TSaveDialog;
    SaveDialog2: TSaveDialog;
    ICO1: TMenuItem;
    SaveDialog3: TSaveDialog;
    ReadMetxt1: TMenuItem;
    N23: TMenuItem;
    TimerImage: TTimer;
    N24: TMenuItem;
    N25: TMenuItem;
    PpMnShell: TPopupMenu;
    N26: TMenuItem;
    N27: TMenuItem;
    N28: TMenuItem;
    N29: TMenuItem;
    N30: TMenuItem;
    N31: TMenuItem;
    N32: TMenuItem;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Edit1KeyPress(Sender: TObject; var Key: Char);
    procedure N4Click(Sender: TObject);
    procedure N6Click(Sender: TObject);
    procedure N13Click(Sender: TObject);
    procedure N11Click(Sender: TObject);
    procedure N12Click(Sender: TObject);
    procedure ShellTreeView1Change(Sender: TObject; Node: TTreeNode);
    procedure FileListBox1Click(Sender: TObject);
    procedure N5Click(Sender: TObject);
    procedure FileListBox1DblClick(Sender: TObject);
    procedure Image1MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Image1MouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure Image1MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure N251Click(Sender: TObject);
    procedure N501Click(Sender: TObject);
    procedure N2001Click(Sender: TObject);
    procedure N4002Click(Sender: TObject);
    procedure N8001Click(Sender: TObject);
    procedure FormMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure N8Click(Sender: TObject);
    procedure N15Click(Sender: TObject);
    procedure N16Click(Sender: TObject);
    procedure N17Click(Sender: TObject);
    procedure N18Click(Sender: TObject);
    procedure N19Click(Sender: TObject);
    procedure N21Click(Sender: TObject);
    procedure TimerShowTimer(Sender: TObject);
    procedure JPEG1Click(Sender: TObject);
    procedure ICO1Click(Sender: TObject);
    procedure FilterComboBox1Change(Sender: TObject);
    procedure Edit1Click(Sender: TObject);
    procedure TimerImageTimer(Sender: TObject);
    procedure ReadMetxt1Click(Sender: TObject);
    procedure N24Click(Sender: TObject);
    procedure N29Click(Sender: TObject);
    procedure N30Click(Sender: TObject);
    procedure N31Click(Sender: TObject);
  private
    { Private declarations }
  public
    cfg:TIniFile;
    WinXpStyle, Moving, CanRedraw: Boolean;
    x0, y0, x1, y1: Integer;
    Scale: Real;
    CurrentPath: String;
    procedure SearchFileByName;
    procedure NextPicture;
    procedure PrevPicture;
    procedure FirstPicture;
    procedure LastPicture;
    procedure TimerRandomPic;
    procedure TimerNextPic;
    procedure TimerPrevPic;
    procedure LoadPicture(Path: String);
    procedure UnloadPicture;
    procedure SetScale(Value: Real);
    procedure ViewScale(Value: Integer); //Value: 0-auto -1-don't show
    procedure ShowPropertiesDialog(Value: String);
    { Public declarations }
  end;

var
  FormMain: TFormMain;

implementation

uses uZoom, uAbout, uOptions, uJPEGPic;

{$R *.dfm}
{$R winxpstyle.res}


procedure TFormMain.SearchFileByName;
begin
if FileExists(ShellTreeView1.Path+'\'+Edit1.Text)then
  begin
  FilterComboBox1.ItemIndex:=0;
  FileListBox1.Repaint;
  FileListBox1.ApplyFilePath(Edit1.Text);
  end else
  begin
  if FileExists(ShellTreeView1.Path+'\'+Edit1.Text+'.jpg')then
    begin
    FilterComboBox1.ItemIndex:=1;
    FileListBox1.ApplyFilePath(Edit1.Text+'.jpg');
    end else
    begin
    if FileExists(ShellTreeView1.Path+'\'+Edit1.Text+'.jpeg')then
      begin
      FilterComboBox1.ItemIndex:=2;
      FileListBox1.ApplyFilePath(Edit1.Text+'.jpeg');
      end else
      begin
      if FileExists(ShellTreeView1.Path+'\'+Edit1.Text+'.bmp')then
        begin
        FilterComboBox1.ItemIndex:=3;
        FileListBox1.ApplyFilePath(Edit1.Text+'.bmp');
        end else
        begin
        if FileExists(ShellTreeView1.Path+'\'+Edit1.Text+'.ico')then
          begin
          FilterComboBox1.ItemIndex:=4;
          FileListBox1.ApplyFilePath(Edit1.Text+'.ico');
          end else
          begin
          if FileExists(ShellTreeView1.Path+'\'+Edit1.Text+'.tga')then
            begin
            FilterComboBox1.ItemIndex:=5;
            FileListBox1.Update;
            FileListBox1.ApplyFilePath(Edit1.Text+'.tga');
            end;
          end;
        end;
      end;
    end;
  end;
LoadPicture(FileListBox1.FileName);
end;

procedure TFormMain.NextPicture;
begin
if FileListBox1.ItemIndex = FileListBox1.Count-1 then
  begin
  if FormOptions.CheckBox1.Checked then
    begin
    FileListBox1.ItemIndex:=0;
    LoadPicture(FileListBox1.FileName);
    end;
  end else
  begin
  FileListBox1.ItemIndex:=FileListBox1.ItemIndex + 1;
  LoadPicture(FileListBox1.FileName);
  end;
end;

procedure TFormMain.PrevPicture;
begin
if FileListBox1.ItemIndex <= 0 then
  begin
  if FormOptions.CheckBox1.Checked then
    begin
    FileListBox1.ItemIndex:=FileListBox1.Count-1;
    LoadPicture(FileListBox1.FileName);
    end;
  end else
  begin
  FileListBox1.ItemIndex:=FileListBox1.ItemIndex - 1;
  LoadPicture(FileListBox1.FileName);
  end;
end;

procedure TFormMain.FirstPicture;
begin
if FileListBox1.Count <> 0 then
  begin
  FileListBox1.ItemIndex:=0;
  end;
LoadPicture(FileListBox1.FileName);
end;

procedure TFormMain.LastPicture;
begin
if FileListBox1.Count <> 0 then
  begin
  FileListBox1.ItemIndex:=FileListBox1.Count-1;
  end;
LoadPicture(FileListBox1.FileName);
end;

procedure TFormMain.TimerNextPic;
begin
if FileListBox1.ItemIndex = FileListBox1.Count-1 then
  begin
  if FormOptions.CheckBox2.Checked then
    begin
    FileListBox1.ItemIndex:=0;
    LoadPicture(FileListBox1.FileName);
    end else TimerShow.Enabled:=False;
  end else
  begin
  FileListBox1.ItemIndex:=FileListBox1.ItemIndex + 1;
  LoadPicture(FileListBox1.FileName);
  end;
end;

procedure TFormMain.TimerPrevPic;
begin
if FileListBox1.ItemIndex <= 0 then
  begin
  if FormOptions.CheckBox2.Checked then
    begin
    FileListBox1.ItemIndex:=FileListBox1.Count-1;
    LoadPicture(FileListBox1.FileName);
    end else TimerShow.Enabled:=False;
  end else
  begin
  FileListBox1.ItemIndex:=FileListBox1.ItemIndex - 1;
  LoadPicture(FileListBox1.FileName);
  end;
end;

procedure TFormMain.TimerRandomPic;
begin
FileListBox1.ItemIndex:=Random(FileListBox1.Count);
LoadPicture(FileListBox1.FileName);
end;

procedure TFormMain.LoadPicture(Path: String);
var PicWidth, PicHeight: Integer;
    Ext: String;
begin
try
  if FormOptions.CheckBox4.Checked then
    begin
    if FormZoom.Showing then
      begin
      FormZoom.Image1.Picture.LoadFromFile(Path);
      Image1.Picture:=nil;
      FormZoom.SetScale(FormZoom.Scale);
      FormZoom.ViewScale(0);
      ViewScale(-1);
      PicWidth:=FormZoom.Image1.Picture.Width;
      PicHeight:=FormZoom.Image1.Picture.Height;

      StatusBar1.Panels[0].Text:='';
      StatusBar1.Panels[2].Text:='';
      end else
      begin
      Image1.Picture.LoadFromFile(Path);
      FormZoom.Image1.Picture:=nil;
      SetScale(Scale);
      ViewScale(0);
      FormZoom.ViewScale(0);
      PicWidth:=Image1.Picture.Width;
      PicHeight:=Image1.Picture.Height;

      StatusBar1.Panels[0].Text:=IntToStr(FileListBox1.ItemIndex+1)+'/'+IntToStr(FileListBox1.Count);
      StatusBar1.Panels[2].Text:=IntToStr(PicWidth)+' x '+IntToStr(PicHeight);
      end;
    end else
    begin
    Image1.Picture.LoadFromFile(Path);
    FormZoom.Image1.Picture.LoadFromFile(Path);
    SetScale(Scale);
    ViewScale(0);
    FormZoom.SetScale(FormZoom.Scale);
    FormZoom.ViewScale(0);
    PicWidth:=Image1.Picture.Width;
    PicHeight:=Image1.Picture.Height;

    StatusBar1.Panels[0].Text:=IntToStr(FileListBox1.ItemIndex+1)+'/'+IntToStr(FileListBox1.Count);
    StatusBar1.Panels[2].Text:=IntToStr(PicWidth)+' x '+IntToStr(PicHeight);
    end;

  StatusBar1.Panels[3].Text:=ExtractFileName(Path);
  StatusBar1.Panels[4].Text:=ExtractFilePath(Path);
  FormZoom.StatusBar1.Panels[0].Text:=IntToStr(FileListBox1.ItemIndex+1)+'/'+IntToStr(FileListBox1.Count);
  FormZoom.StatusBar1.Panels[2].Text:=IntToStr(PicWidth)+' x '+IntToStr(PicHeight);
  FormZoom.StatusBar1.Panels[3].Text:=StatusBar1.Panels[3].Text;

  CurrentPath:=Path;

  Edit1.Text:=ExtractFileName(Path);
  except
  UnloadPicture;
  end;

Ext:=LowerCase(ExtractFileExt(Path));
if(Ext = '.jpg')or(Ext = '.jpeg')or(Ext = '.bmp')or(Ext = '.tga')then
  begin
  JPEG1.Enabled:=True;
  ICO1.Enabled:=False;
  end else
  begin
  if Ext = '.ico'
    then ICO1.Enabled:=True
    else
      begin
      ICO1.Enabled:=False;
      UnloadPicture;
      end;
  JPEG1.Enabled:=False;
  end;
end;

procedure TFormMain.UnloadPicture;
begin
try
  Edit1.Text:='';
  Image1.Picture:=nil;
  FormZoom.Image1.Picture:=nil;
  CurrentPath:='';
  StatusBar1.Panels[0].Text:='';
  StatusBar1.Panels[1].Text:='';
  StatusBar1.Panels[2].Text:='';
  StatusBar1.Panels[3].Text:='';
  FormZoom.StatusBar1.Panels[0].Text:='';
  FormZoom.StatusBar1.Panels[1].Text:='';
  FormZoom.StatusBar1.Panels[2].Text:='';
  FormZoom.StatusBar1.Panels[3].Text:='';
  except end;
end;

procedure TFormMain.SetScale(Value: Real);
begin
Scale:=Value;
if (not N11.Checked)and(not N12.Checked)and(not N8.Checked) then
  begin
  Image1.Width:=Round(Image1.Picture.Width * Value);
  Image1.Height:=Round(Image1.Picture.Height * Value);
  end else
  begin
  Image1.Width:=Panel1.Width;
  Image1.Height:=Panel1.Height;
  end;
Image1.Top:=0;
Image1.Left:=0;
end;

procedure TFormMain.ViewScale(Value: Integer);
var SourceProportion, ImProportion: Real;
    Ext: String;
begin
Ext:=LowerCase(ExtractFileExt(FileListBox1.FileName));
if(Ext = '.jpg')or(Ext = '.jpeg')or(Ext = '.bmp')or(Ext = '.tga')then
  begin
  case Value of
    -1: StatusBar1.Panels[1].Text:='';
    0:
      begin
      if N11.Checked then
        begin
        SourceProportion:=Image1.Picture.Width / Image1.Picture.Height;
        ImProportion:=Image1.Width / Image1.Height;
        if SourceProportion > ImProportion
          then StatusBar1.Panels[1].Text:=IntToStr(Round(Image1.Width / Image1.Picture.Width * 100))+'%'
          else StatusBar1.Panels[1].Text:=IntToStr(Round(Image1.Height / Image1.Picture.Height * 100))+'%';
        end;
      end;
      else StatusBar1.Panels[1].Text:=IntToStr(Value)+'%';
    end;
  end else
  begin
  StatusBar1.Panels[1].Text:='';
  end;
end;

procedure TFormMain.ShowPropertiesDialog(Value: String);
var SExInfo: TSHellExecuteInfo;
begin
ZeroMemory(Addr(SExInfo),SizeOf(SExInfo));
SExInfo.cbSize:=SizeOf(SExInfo);
SExInfo.lpFile:=PChar(Value);
SExInfo.lpVerb:='properties';
SExInfo.fMask:=SEE_MASK_INVOKEIDLIST;
ShellExecuteEx(Addr(SExInfo));
end;

procedure TFormMain.FormCreate(Sender: TObject);
begin
cfg:=Tinifile.Create(ExtractFilePath(ParamStr(0))+'SimpleView.ini');

Randomize;

Scale:=1;

Top:=cfg.ReadInteger('form','top',172);
Left:=cfg.ReadInteger('form','left',150);
end;

procedure TFormMain.FormClose(Sender: TObject; var Action: TCloseAction);
begin
cfg.WriteInteger('form','top',Top);
cfg.WriteInteger('form','left',Left);

cfg.WriteInteger('program','itemindex',FormMain.FileListBox1.ItemIndex);

cfg.WriteInteger('options','color1',FormOptions.ColorBox1.Selected);
cfg.WriteInteger('options','color2',FormOptions.ColorBox2.Selected);

cfg.WriteBool('options','startviewtype',FormOptions.RadioButton2.Checked);
cfg.WriteString('options','startfolder',FormOptions.Edit1.Text);
cfg.WriteString('options','lastfolder',ShellTreeView1.Path);
cfg.WriteBool('options','onlyonewin',FormOptions.CheckBox4.Checked);
cfg.WriteBool('options','loopview',FormOptions.CheckBox1.Checked);

cfg.WriteInteger('options','slidesh_delay',FormOptions.SpinEdit1.Value);
if FormOptions.RadioButton3.Checked then
  begin
  cfg.WriteInteger('options','slidesh_order',0);
  end else
  begin
  if FormOptions.RadioButton4.Checked then
    begin
    cfg.WriteInteger('options','slidesh_order',1);
    end else
    begin
    cfg.WriteInteger('options','slidesh_order',2);
    end;
  end;
cfg.WriteBool('options','slidesh_loop',FormOptions.CheckBox2.Checked);
cfg.WriteBool('options','slidesh_startfrbeg',FormOptions.CheckBox3.Checked);

if FormZoom.FormStyle = fsNormal
  then cfg.WriteBool('formzoom','stayontop',False)
  else cfg.WriteBool('formzoom','stayontop',True);

end;

procedure TFormMain.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
if (Key = 13)and(not Edit1.Focused) then
  begin
  FileListBox1DblClick(FileListBox1);
  end;
end;

// Menu

procedure TFormMain.N4Click(Sender: TObject);      //Close
begin
Close;
end;

procedure TFormMain.JPEG1Click(Sender: TObject);    //Save as jpeg
var JpegIm: TJpegImage;
begin
if SaveDialog2.Execute then
  begin
  FormJPEG.ShowModal;
  if FormJPEG.ModalResult = idOK then
    begin
    JpegIm:=TJpegImage.Create;
    if FormZoom.Showing then
      begin
      JpegIm.Assign(FormZoom.Image1.Picture.Graphic);
      end else
      begin
      JpegIm.Assign(Image1.Picture.Graphic);
      end;
    JpegIm.CompressionQuality:=FormJPEG.CompValue;
    JpegIm.Compress;
    JpegIm.SaveToFile(SaveDialog2.FileName);
    JpegIm.Free;
    end;
  N24Click(N24);
  end;
end;

procedure TFormMain.ICO1Click(Sender: TObject);        //Save as ico
begin
if SaveDialog3.Execute then
  begin
  if FormZoom.Showing then
    begin
    FormZoom.Image1.Picture.Icon.SaveToFile(SaveDialog3.FileName)
    end else
    begin
    Image1.Picture.Icon.SaveToFile(SaveDialog3.FileName);
    end;
  N24Click(N24);
  end;
end;

procedure TFormMain.N29Click(Sender: TObject);             //open
begin
try
  ShellExecute(0,'',pchar(FileListBox1.FileName),'','',1);
  except end;
end;

procedure TFormMain.N30Click(Sender: TObject);             //delete
begin
if Application.MessageBox(PChar('������ ����, �� �� ������� ��� ������������. �������?'),PChar(FormMain.Caption),mb_YesNo+mb_IconWarning) = idYes then
  begin
  DeleteFile(FileListBox1.FileName);
  N24Click(N4);
  end;
end;

procedure TFormMain.N31Click(Sender: TObject);               //properties
begin
try
  if FileExists(FileListBox1.FileName) then
    ShowPropertiesDialog(FileListBox1.FileName);
  except end;
end;

procedure TFormMain.N15Click(Sender: TObject);      //Next
begin
NextPicture;
end;

procedure TFormMain.N16Click(Sender: TObject);      //Prev
begin
PrevPicture;
end;

procedure TFormMain.N17Click(Sender: TObject);        //First
begin
FirstPicture;
end;

procedure TFormMain.N18Click(Sender: TObject);       //Last
begin
LastPicture;
end;

procedure TFormMain.N19Click(Sender: TObject);        //Start SlideShow
begin
if FormOptions.CheckBox3.Checked then
  begin
  if FormOptions.RadioButton4.Checked
    then LastPicture
    else FirstPicture;
  end;

TimerShow.Interval:=FormOptions.SpinEdit1.Value * 1000;
TimerShow.Enabled:=True;
end;

procedure TFormMain.N21Click(Sender: TObject);       //Stop SlideShow
begin
TimerShow.Enabled:=False;
end;

procedure TFormMain.N11Click(Sender: TObject);     //Fit Image
begin
N11.Checked:=True;
Image1.Proportional:=True;
Image1.Stretch:=True;
MenuItem1.Checked:=True;
SetScale(0);
ViewScale(0);
end;

procedure TFormMain.N12Click(Sender: TObject);     //Stretch
begin
N12.Checked:=True;
MenuItem2.Checked:=True;
Image1.Proportional:=False;
Image1.Stretch:=True;
SetScale(0);
ViewScale(-1);
end;

procedure TFormMain.N8Click(Sender: TObject);     //Center
begin
N8.Checked:=True;
N9.Checked:=True;
Image1.Proportional:=False;
Image1.Stretch:=False;
SetScale(0);
ViewScale(100);
end;

procedure TFormMain.N251Click(Sender: TObject);    //25%
begin
N251.Checked:=True;
N252.Checked:=True;
Image1.Proportional:=True;
Image1.Stretch:=False;
SetScale(0.25);
ViewScale(25);
end;

procedure TFormMain.N501Click(Sender: TObject);   //50%
begin
N501.Checked:=True;
N502.Checked:=True;
Image1.Proportional:=True;
Image1.Stretch:=False;
SetScale(0.5);
ViewScale(50);
end;

procedure TFormMain.N13Click(Sender: TObject);    //100%
begin
N13.Checked:=True;
MenuItem3.Checked:=True;
Image1.Proportional:=False;
Image1.Stretch:=False;
SetScale(1);
ViewScale(100);
end;

procedure TFormMain.N2001Click(Sender: TObject);    //200%
begin
N2001.Checked:=True;
N2002.Checked:=True;
Image1.Proportional:=True;
Image1.Stretch:=True;
SetScale(2);
ViewScale(200);
end;

procedure TFormMain.N4002Click(Sender: TObject);    //400%
begin
N4002.Checked:=True;
N4003.Checked:=True;
Image1.Proportional:=True;
Image1.Stretch:=True;
SetScale(4);
ViewScale(400);
end;

procedure TFormMain.N8001Click(Sender: TObject);    //800%
begin
N8001.Checked:=True;
N8002.Checked:=True;
Image1.Proportional:=True;
Image1.Stretch:=True;
SetScale(8);
ViewScale(800);
end;

procedure TFormMain.N24Click(Sender: TObject);      //Refresh
var Ind: Integer;
begin
Ind:=FileListBox1.ItemIndex;
ShellTreeView1.Update;
FileListBox1.Update;
if Ind = FileListBox1.Count then Ind:=Ind-1;
FileListBox1.ItemIndex:=Ind;
LoadPicture(FileListBox1.FileName);
end;

procedure TFormMain.N5Click(Sender: TObject);        //Options
begin
FormOptions.ShowModal;
end;

procedure TFormMain.ReadMetxt1Click(Sender: TObject);    //Readme...
begin
if FileExists(ExtractFilePath(ParamStr(0))+'ReadMe.txt')
  then ShellExecute(0,'',pchar(extractfilepath(ParamStr(0))+'ReadMe.txt'),'','',1)
  else Application.MessageBox(PChar('������ ��� ������ ����� ��� ���� �� ������'
    +#13+ExtractFilePath(ParamStr(0))+'ReadMe.txt'),PChar(FormMain.Caption),MB_ICONERROR+mb_OK);
end;

procedure TFormMain.N6Click(Sender: TObject);         //About...
begin
FormAbout.ShowModal;
end;

// End Menu

procedure TFormMain.Edit1KeyPress(Sender: TObject; var Key: Char);
begin
if Key = #13 then
  begin
  SearchFileByName;
  FileListBox1.SetFocus;
  Key:=#0;
  end;
end;

procedure TFormMain.ShellTreeView1Change(Sender: TObject; Node: TTreeNode);
begin
UnloadPicture;
FileListBox1.Directory:=ShellTreeView1.Path;
if FileListBox1.Count <> 0 then
  begin
  FileListBox1.ItemIndex:=0;
  LoadPicture(FileListBox1.FileName);
  end;
end;

procedure TFormMain.FileListBox1Click(Sender: TObject);
begin
LoadPicture(FileListBox1.FileName);
end;

procedure TFormMain.FileListBox1DblClick(Sender: TObject);
begin
FormZoom.WindowState:=wsMaximized;
FormZoom.Show;
LoadPicture(FileListBox1.FileName);
end;

procedure TFormMain.Image1MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
if (Button = mbLeft)and(not N11.Checked)and(not N12.Checked)and(not N8.Checked)then
  begin
  TimerImage.Enabled:=True;
  Moving:=True;
  x0:=x;
  y0:=y;
  x1:=x;
  y1:=y;
  end else Moving:=False;
end;

procedure TFormMain.Image1MouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
begin
if Moving then
  begin
  Screen.Cursor:=crSizeAll;
  x1:=x;
  y1:=y;
  CanRedraw:=True;
  end;
end;

procedure TFormMain.Image1MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
TimerImage.Enabled:=False;
Moving:=False;
Screen.Cursor:=crDefault;
end;

procedure TFormMain.TimerImageTimer(Sender: TObject);
begin
if CanRedraw then
  begin
  Image1.Left:=Image1.Left + x1 - x0;
  if Image1.Left <= Panel1.Width-Image1.Width then
    begin
    Image1.Left:=-(Image1.Width-Panel1.Width);
    end;
  if Image1.Left >= 0 then
    begin
    Image1.Left:=0;
    end;

  Image1.Top:=Image1.Top + y1 - y0;
  if Image1.Top <= Panel1.Height-Image1.Height then
    begin
    Image1.Top:=-(Image1.Height-Panel1.Height);
    end;
  if Image1.Top >= 0 then
    begin
    Image1.Top:=0;
    end;

  CanRedraw:=False;
  end;
end;

procedure TFormMain.FormMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
Moving:=False;
Screen.Cursor:=crDefault;
end;

procedure TFormMain.TimerShowTimer(Sender: TObject);
begin
if FormOptions.RadioButton3.Checked then
  begin
  TimerNextPic;
  end else
  begin
  if FormOptions.RadioButton4.Checked then
    begin
    TimerPrevPic;
    end else
    begin
    TimerRandomPic;
    end;
  end;
end;

procedure TFormMain.FilterComboBox1Change(Sender: TObject);
begin
if FileListBox1.Count <> 0 then
  begin
  FileListBox1.ItemIndex:=0;
  LoadPicture(FileListBox1.FileName);
  end else
  begin
  UnloadPicture;
  end;
FileListBox1.SetFocus;
end;

procedure TFormMain.Edit1Click(Sender: TObject);
begin
Edit1.SelectAll;
end;

end.
