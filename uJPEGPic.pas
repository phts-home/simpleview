unit uJPEGPic;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls;

type
  TFormJPEG = class(TForm)
    Button1: TButton;
    Button2: TButton;
    TrackBar1: TTrackBar;
    Label1: TLabel;
    Label2: TLabel;
    procedure TrackBar1Change(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    CompValue: Integer;
    { Public declarations }
  end;

var
  FormJPEG: TFormJPEG;

implementation

{$R *.dfm}

procedure TFormJPEG.TrackBar1Change(Sender: TObject);
begin
CompValue:=TrackBar1.Position * 10;
Label2.Caption:=IntToStr(CompValue)+'%';
if CompValue = 0 then
  begin
  CompValue:=1;
  end;
end;

procedure TFormJPEG.FormClose(Sender: TObject; var Action: TCloseAction);
begin
CompValue:=TrackBar1.Position * 10;
end;

procedure TFormJPEG.FormCreate(Sender: TObject);
begin
CompValue:=70;
end;

end.
