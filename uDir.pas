unit uDir;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, ShellCtrls;

type
  TFormDir = class(TForm)
    ShellTreeView1: TShellTreeView;
    Button1: TButton;
    Button2: TButton;
    procedure FormResize(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormDir: TFormDir;

implementation

uses uOptions;

{$R *.dfm}

procedure TFormDir.FormResize(Sender: TObject);
begin
ShellTreeView1.Width:=FormDir.ClientWidth - 30;
ShellTreeView1.Height:=FormDir.ClientHeight - 83;
Button1.Top:=FormDir.ClientHeight - 39;
Button1.Left:=FormDir.ClientWidth - 101;
Button2.Top:=Button1.Top;
Button2.Left:=FormDir.ClientWidth - 196;
end;

procedure TFormDir.FormShow(Sender: TObject);
begin
Top:=FormOptions.Left - 44;
Top:=FormOptions.Top + 26;
end;

end.
