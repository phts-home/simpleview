object FormZoom: TFormZoom
  Left = 161
  Top = 220
  Width = 329
  Height = 319
  HorzScrollBar.Visible = False
  VertScrollBar.Visible = False
  Caption = 'SimpleView'
  Color = clBtnFace
  Constraints.MinHeight = 200
  Constraints.MinWidth = 200
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  Menu = MainMenu1
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  OnMouseUp = FormMouseUp
  OnMouseWheelDown = FormMouseWheelDown
  OnMouseWheelUp = FormMouseWheelUp
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object StatusBar1: TStatusBar
    Left = 0
    Top = 248
    Width = 321
    Height = 21
    Align = alCustom
    Panels = <
      item
        Alignment = taCenter
        Width = 50
      end
      item
        Alignment = taCenter
        Width = 50
      end
      item
        Alignment = taCenter
        Width = 75
      end
      item
        Width = 50
      end>
  end
  object Panel1: TPanel
    Left = 5
    Top = 5
    Width = 311
    Height = 236
    BevelOuter = bvNone
    PopupMenu = PopupMenu1
    TabOrder = 1
    OnDblClick = Image1DblClick
    object Image1: TImage
      Left = 0
      Top = 0
      Width = 105
      Height = 105
      Center = True
      PopupMenu = PopupMenu1
      Proportional = True
      Stretch = True
      OnDblClick = Image1DblClick
      OnMouseDown = Image1MouseDown
      OnMouseMove = Image1MouseMove
      OnMouseUp = Image1MouseUp
    end
  end
  object MainMenu1: TMainMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Left = 10
    Top = 10
    object N9: TMenuItem
      Caption = #1055#1088#1086#1089#1084#1086#1090#1088
      object N21: TMenuItem
        AutoCheck = True
        Caption = #1054#1089#1090#1072#1074#1072#1090#1100#1089#1103' '#1085#1072#1074#1077#1088#1093#1091
        OnClick = N21Click
      end
      object N22: TMenuItem
        Caption = '-'
      end
      object N10: TMenuItem
        Caption = #1057#1083#1077#1076#1091#1097#1072#1103
        ShortCut = 34
        OnClick = N10Click
      end
      object N11: TMenuItem
        Caption = #1055#1088#1077#1076#1099#1076#1091#1097#1072#1103
        ShortCut = 33
        OnClick = N11Click
      end
      object N13: TMenuItem
        Caption = #1055#1077#1088#1074#1072#1103
        ShortCut = 36
        OnClick = N13Click
      end
      object N12: TMenuItem
        Caption = #1055#1086#1089#1083#1077#1076#1085#1103#1103
        ShortCut = 35
        OnClick = N12Click
      end
      object N18: TMenuItem
        Caption = '-'
      end
      object N19: TMenuItem
        Caption = #1053#1072#1095#1072#1090#1100' '#1087#1086#1082#1072#1079
        ShortCut = 120
        OnClick = N19Click
      end
      object N20: TMenuItem
        Caption = #1047#1072#1082#1086#1085#1095#1080#1090#1100' '#1087#1086#1082#1072#1079
        ShortCut = 16504
        OnClick = N20Click
      end
    end
    object N1: TMenuItem
      Caption = #1048#1079#1086#1073#1088#1072#1078#1077#1085#1080#1077
      object N2: TMenuItem
        AutoCheck = True
        Caption = #1055#1086#1076#1086#1075#1085#1072#1090#1100' '#1087#1086#1076' '#1088#1072#1079#1084#1077#1088
        Checked = True
        RadioItem = True
        ShortCut = 16432
        OnClick = N2Click
      end
      object N3: TMenuItem
        AutoCheck = True
        Caption = #1056#1072#1089#1090#1103#1085#1091#1090#1100
        RadioItem = True
        ShortCut = 16441
        OnClick = N3Click
      end
      object N7: TMenuItem
        AutoCheck = True
        Caption = #1055#1086' '#1094#1077#1085#1090#1088#1091
        RadioItem = True
        ShortCut = 16440
        OnClick = N7Click
      end
      object N8: TMenuItem
        Caption = '-'
      end
      object N251: TMenuItem
        AutoCheck = True
        Caption = '25%'
        RadioItem = True
        ShortCut = 16433
        OnClick = N251Click
      end
      object N5: TMenuItem
        AutoCheck = True
        Caption = '50%'
        RadioItem = True
        ShortCut = 16434
        OnClick = N5Click
      end
      object N4: TMenuItem
        AutoCheck = True
        Caption = '100%'
        RadioItem = True
        ShortCut = 16435
        OnClick = N4Click
      end
      object N6: TMenuItem
        AutoCheck = True
        Caption = '200%'
        RadioItem = True
        ShortCut = 16436
        OnClick = N6Click
      end
      object N3001: TMenuItem
        AutoCheck = True
        Caption = '400%'
        RadioItem = True
        ShortCut = 16437
        OnClick = N3001Click
      end
      object N8001: TMenuItem
        AutoCheck = True
        Caption = '800%'
        RadioItem = True
        ShortCut = 16438
        OnClick = N8001Click
      end
    end
  end
  object PopupMenu1: TPopupMenu
    Left = 40
    Top = 10
    object N14: TMenuItem
      AutoCheck = True
      Caption = #1055#1086#1076#1086#1075#1085#1072#1090#1100' '#1087#1086#1076' '#1088#1072#1079#1084#1077#1088
      Checked = True
      RadioItem = True
      ShortCut = 16432
      OnClick = N2Click
    end
    object N15: TMenuItem
      AutoCheck = True
      Caption = #1056#1072#1089#1090#1103#1085#1091#1090#1100
      RadioItem = True
      ShortCut = 16441
      OnClick = N3Click
    end
    object N16: TMenuItem
      AutoCheck = True
      Caption = #1055#1086' '#1094#1077#1085#1090#1088#1091
      RadioItem = True
      ShortCut = 16440
      OnClick = N7Click
    end
    object N17: TMenuItem
      AutoCheck = True
      Caption = '-'
    end
    object N252: TMenuItem
      AutoCheck = True
      Caption = '25%'
      RadioItem = True
      ShortCut = 16433
      OnClick = N251Click
    end
    object N501: TMenuItem
      AutoCheck = True
      Caption = '50%'
      RadioItem = True
      ShortCut = 16434
      OnClick = N5Click
    end
    object N1001: TMenuItem
      AutoCheck = True
      Caption = '100%'
      RadioItem = True
      ShortCut = 16435
      OnClick = N4Click
    end
    object N2001: TMenuItem
      AutoCheck = True
      Caption = '200%'
      RadioItem = True
      ShortCut = 16436
      OnClick = N6Click
    end
    object N4001: TMenuItem
      AutoCheck = True
      Caption = '400%'
      RadioItem = True
      ShortCut = 16437
      OnClick = N3001Click
    end
    object N8002: TMenuItem
      AutoCheck = True
      Caption = '800%'
      RadioItem = True
      ShortCut = 16438
      OnClick = N8001Click
    end
  end
  object Timer1: TTimer
    Interval = 250
    OnTimer = Timer1Timer
    Left = 70
    Top = 10
  end
end
