object FormOptions: TFormOptions
  Left = 247
  Top = 155
  HorzScrollBar.Visible = False
  VertScrollBar.Visible = False
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1080
  ClientHeight = 258
  ClientWidth = 370
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Button1: TButton
    Left = 285
    Top = 220
    Width = 75
    Height = 25
    Caption = #1047#1072#1082#1088#1099#1090#1100
    ModalResult = 1
    TabOrder = 0
  end
  object PageControl1: TPageControl
    Left = 10
    Top = 5
    Width = 351
    Height = 203
    ActivePage = TabSheet1
    TabOrder = 1
    object TabSheet1: TTabSheet
      Caption = #1048#1085#1090#1077#1088#1092#1077#1081#1089
      object Label1: TLabel
        Left = 15
        Top = 20
        Width = 130
        Height = 13
        Caption = #1062#1074#1077#1090' '#1092#1086#1085#1072' '#1075#1083#1072#1074#1085#1086#1075#1086' '#1086#1082#1085#1072
      end
      object Label2: TLabel
        Left = 15
        Top = 45
        Width = 133
        Height = 13
        Caption = #1062#1074#1077#1090' '#1092#1086#1085#1072' '#1087#1088#1086#1089#1084#1086#1090#1088#1097#1080#1082#1072
      end
      object ColorBox1: TColorBox
        Left = 181
        Top = 15
        Width = 145
        Height = 22
        DefaultColorColor = clBtnFace
        NoneColorColor = clBtnFace
        Selected = clBtnFace
        ItemHeight = 16
        TabOrder = 0
        OnChange = ColorBox1Change
      end
      object ColorBox2: TColorBox
        Left = 181
        Top = 40
        Width = 145
        Height = 22
        DefaultColorColor = clBtnFace
        NoneColorColor = clBtnFace
        Selected = clBtnFace
        ItemHeight = 16
        TabOrder = 1
        OnChange = ColorBox2Change
      end
    end
    object TabSheet2: TTabSheet
      Caption = #1055#1088#1086#1089#1084#1086#1090#1088
      ImageIndex = 1
      object GroupBox1: TGroupBox
        Left = 15
        Top = 14
        Width = 311
        Height = 92
        Caption = ' '#1053#1072#1095#1080#1085#1072#1090#1100' '#1087#1088#1086#1089#1084#1086#1090#1088' '
        TabOrder = 0
        object SpeedButton1: TSpeedButton
          Left = 275
          Top = 60
          Width = 23
          Height = 22
          Caption = '...'
          OnClick = SpeedButton1Click
        end
        object RadioButton1: TRadioButton
          Left = 10
          Top = 20
          Width = 286
          Height = 17
          Caption = #1057' '#1087#1086#1089#1083#1077#1076#1085#1077#1081' '#1087#1072#1087#1082#1080
          Checked = True
          TabOrder = 0
          TabStop = True
        end
        object RadioButton2: TRadioButton
          Left = 10
          Top = 40
          Width = 286
          Height = 17
          Caption = #1042#1099#1073#1088#1072#1090#1100':'
          TabOrder = 1
        end
        object Edit1: TEdit
          Left = 10
          Top = 60
          Width = 256
          Height = 21
          TabOrder = 2
        end
      end
      object CheckBox1: TCheckBox
        Left = 15
        Top = 140
        Width = 311
        Height = 17
        Caption = #1055#1086#1089#1083#1077' '#1076#1086#1089#1090#1080#1078#1077#1085#1080#1103' '#1082#1086#1085#1094#1072' '#1087#1077#1088#1077#1093#1086#1076#1080#1090#1100' '#1085#1072' '#1085#1072#1095#1072#1083#1086
        TabOrder = 1
      end
      object CheckBox4: TCheckBox
        Left = 15
        Top = 120
        Width = 311
        Height = 17
        Caption = #1055#1088#1086#1089#1084#1086#1090#1088' '#1090#1086#1083#1100#1082#1086' '#1074' '#1086#1076#1085#1086#1084' '#1086#1082#1085#1077
        TabOrder = 2
      end
    end
    object TabSheet3: TTabSheet
      Caption = #1057#1083#1072#1081#1076' '#1096#1086#1091
      ImageIndex = 2
      object Label3: TLabel
        Left = 15
        Top = 20
        Width = 115
        Height = 13
        Caption = #1047#1072#1076#1077#1088#1078#1082#1072' ('#1074' '#1089#1077#1082#1091#1085#1076#1072#1093')'
      end
      object SpinEdit1: TSpinEdit
        Left = 275
        Top = 15
        Width = 51
        Height = 22
        MaxValue = 10
        MinValue = 1
        TabOrder = 0
        Value = 3
      end
      object GroupBox2: TGroupBox
        Left = 15
        Top = 45
        Width = 311
        Height = 61
        Caption = ' '#1053#1072#1087#1088#1072#1074#1083#1077#1085#1080#1077' '
        TabOrder = 1
        object RadioButton3: TRadioButton
          Left = 15
          Top = 25
          Width = 81
          Height = 17
          Caption = #1042#1087#1077#1088#1077#1076
          Checked = True
          TabOrder = 0
          TabStop = True
        end
        object RadioButton4: TRadioButton
          Left = 110
          Top = 25
          Width = 81
          Height = 17
          Caption = #1053#1072#1079#1072#1076
          TabOrder = 1
        end
        object RadioButton5: TRadioButton
          Left = 200
          Top = 25
          Width = 81
          Height = 17
          Caption = #1057#1083#1091#1095#1072#1081#1085#1086
          TabOrder = 2
        end
      end
      object CheckBox2: TCheckBox
        Left = 15
        Top = 120
        Width = 311
        Height = 17
        Caption = #1055#1086#1074#1090#1086#1088#1103#1090#1100
        TabOrder = 2
      end
      object CheckBox3: TCheckBox
        Left = 15
        Top = 140
        Width = 311
        Height = 17
        Caption = #1053#1072#1095#1080#1085#1072#1090#1100' '#1089' '#1085#1072#1095#1072#1083#1072'/'#1082#1086#1085#1094#1072
        Checked = True
        State = cbChecked
        TabOrder = 3
      end
    end
  end
  object OpenDialog1: TOpenDialog
    FileName = 
      'E:\_Projects_\Projects Borland Delphi\'#1055#1088#1086#1077#1082#1090' SimpleView\SimpleVi' +
      'ew.res'
    Filter = '546|*'
    Left = 250
    Top = 215
  end
end
