unit uOptions;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, Buttons, ComCtrls, Spin;

type
  TFormOptions = class(TForm)
    Button1: TButton;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    ColorBox1: TColorBox;
    Label1: TLabel;
    ColorBox2: TColorBox;
    Label2: TLabel;
    GroupBox1: TGroupBox;
    RadioButton1: TRadioButton;
    RadioButton2: TRadioButton;
    Edit1: TEdit;
    SpeedButton1: TSpeedButton;
    OpenDialog1: TOpenDialog;
    CheckBox1: TCheckBox;
    TabSheet3: TTabSheet;
    SpinEdit1: TSpinEdit;
    Label3: TLabel;
    GroupBox2: TGroupBox;
    RadioButton3: TRadioButton;
    RadioButton4: TRadioButton;
    RadioButton5: TRadioButton;
    CheckBox2: TCheckBox;
    CheckBox3: TCheckBox;
    CheckBox4: TCheckBox;
    procedure ColorBox1Change(Sender: TObject);
    procedure ColorBox2Change(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure SpeedButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormOptions: TFormOptions;

implementation

uses uMain, uZoom, uDir;

{$R *.dfm}

procedure TFormOptions.FormCreate(Sender: TObject);
begin
ColorBox1.Selected:=FormMain.cfg.ReadInteger('options','color1',clBtnFace);
FormMain.Color:=ColorBox1.Selected;
ColorBox2.Selected:=FormMain.cfg.ReadInteger('options','color2',clBtnFace);
FormZoom.Color:=ColorBox2.Selected;

RadioButton2.Checked:=FormMain.cfg.ReadBool('options','startviewtype',False);
Edit1.Text:=FormMain.cfg.ReadString('options','startfolder','');
CheckBox4.Checked:=FormMain.cfg.ReadBool('options','onlyonewin',False);
CheckBox1.Checked:=FormMain.cfg.ReadBool('options','loopview',False);

SpinEdit1.Value:=FormMain.cfg.ReadInteger('options','slidesh_delay',3);
case FormMain.cfg.ReadInteger('options','slidesh_order',0) of
  0: RadioButton3.Checked:=True;
  1: RadioButton4.Checked:=True;
  2: RadioButton5.Checked:=True;
  end;
CheckBox2.Checked:=FormMain.cfg.ReadBool('options','slidesh_loop',False);
CheckBox3.Checked:=FormMain.cfg.ReadBool('options','slidesh_startfrbeg',True);

try
  if RadioButton2.Checked
    then FormMain.ShellTreeView1.Path:=Edit1.Text
    else FormMain.ShellTreeView1.Path:=FormMain.cfg.ReadString('options','lastfolder',ExtractFilePath(ParamStr(0)));
  except FormMain.ShellTreeView1.Path:=ExtractFilePath(ParamStr(0));
  end;

FormMain.FileListBox1.ItemIndex:=FormMain.cfg.ReadInteger('program','itemindex',-1);
FormMain.LoadPicture(FormMain.FileListBox1.FileName);
end;

procedure TFormOptions.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
if Edit1.Text='' then RadioButton1.Checked:=True;
end;

procedure TFormOptions.FormShow(Sender: TObject);
begin
Left:=FormMain.Left + 175;
Top:=FormMain.Top + 40;

FormMain.TimerShow.Enabled:=False;
end;

procedure TFormOptions.ColorBox1Change(Sender: TObject);
begin
FormMain.Color:=ColorBox1.Selected;
end;

procedure TFormOptions.ColorBox2Change(Sender: TObject);
begin
FormZoom.Color:=ColorBox2.Selected;
end;

procedure TFormOptions.SpeedButton1Click(Sender: TObject);
begin
FormDir.ShowModal;
if FormDir.ModalResult = idOk then
  begin
  Edit1.Text:=FormDir.ShellTreeView1.Path;
  end;
end;

end.
