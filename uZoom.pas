unit uZoom;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, Menus, TGA, StdCtrls, ComCtrls;

type
  TFormZoom = class(TForm)
    MainMenu1: TMainMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    N5: TMenuItem;
    N6: TMenuItem;
    N3001: TMenuItem;
    N251: TMenuItem;
    N8: TMenuItem;
    N8001: TMenuItem;
    StatusBar1: TStatusBar;
    N7: TMenuItem;
    Panel1: TPanel;
    Image1: TImage;
    N9: TMenuItem;
    N10: TMenuItem;
    N11: TMenuItem;
    N12: TMenuItem;
    N13: TMenuItem;
    PopupMenu1: TPopupMenu;
    N14: TMenuItem;
    N15: TMenuItem;
    N16: TMenuItem;
    N17: TMenuItem;
    N252: TMenuItem;
    N501: TMenuItem;
    N1001: TMenuItem;
    N2001: TMenuItem;
    N4001: TMenuItem;
    N8002: TMenuItem;
    N18: TMenuItem;
    N19: TMenuItem;
    N20: TMenuItem;
    N21: TMenuItem;
    N22: TMenuItem;
    Timer1: TTimer;
    procedure FormShow(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormMouseWheelDown(Sender: TObject; Shift: TShiftState;
      MousePos: TPoint; var Handled: Boolean);
    procedure FormMouseWheelUp(Sender: TObject; Shift: TShiftState;
      MousePos: TPoint; var Handled: Boolean);
    procedure Image1DblClick(Sender: TObject);
    procedure N2Click(Sender: TObject);
    procedure N3Click(Sender: TObject);
    procedure N4Click(Sender: TObject);
    procedure N5Click(Sender: TObject);
    procedure N6Click(Sender: TObject);
    procedure N251Click(Sender: TObject);
    procedure N3001Click(Sender: TObject);
    procedure N8001Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Image1MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Image1MouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure Image1MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FormMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure N7Click(Sender: TObject);
    procedure N10Click(Sender: TObject);
    procedure N11Click(Sender: TObject);
    procedure N13Click(Sender: TObject);
    procedure N12Click(Sender: TObject);
    procedure N19Click(Sender: TObject);
    procedure N20Click(Sender: TObject);
    procedure N21Click(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    Moving, CanRedraw: Boolean;
    x0, y0, x1, y1: Integer;
    Scale: Real;
    procedure SetScale(Value: Real);
    procedure ViewScale(Value: Integer);
    { Public declarations }
  end;

var
  FormZoom: TFormZoom;

implementation

uses uMain, uOptions;

{$R *.dfm}

procedure TFormZoom.SetScale(Value: Real);
begin
Scale:=Value;
if (not N2.Checked)and(not N3.Checked)and(not N7.Checked) then
  begin
  Image1.Width:=Round(Image1.Picture.Width * Value);
  Image1.Height:=Round(Image1.Picture.Height * Value);
  end else
  begin
  Image1.Width:=Panel1.Width;
  Image1.Height:=Panel1.Height;
  end;
Image1.Top:=0;
Image1.Left:=0;
end;

procedure TFormZoom.ViewScale(Value: Integer);
var SourceProportion, ImProportion: Real;
    Ext: String;
begin
Ext:=LowerCase(ExtractFileExt(FormMain.FileListBox1.FileName));
if(Ext = '.jpg')or(Ext = '.jpeg')or(Ext = '.bmp')or(Ext = '.tga')then
  begin      
  case Value of
    -1: StatusBar1.Panels[1].Text:='';
    0:
      begin
      if N2.Checked then
        begin
        SourceProportion:=Image1.Picture.Width / Image1.Picture.Height;
        ImProportion:=Image1.Width / Image1.Height;
        if SourceProportion > ImProportion
          then StatusBar1.Panels[1].Text:=IntToStr(Round(Image1.Width / Image1.Picture.Width * 100))+'%'
          else StatusBar1.Panels[1].Text:=IntToStr(Round(Image1.Height / Image1.Picture.Height * 100))+'%';
        end;
      end;
      else StatusBar1.Panels[1].Text:=IntToStr(Value)+'%';
    end;
  end else
  begin
  StatusBar1.Panels[1].Text:='';
  end;
end;

procedure TFormZoom.FormCreate(Sender: TObject);
begin
N21.Checked:=FormMain.cfg.ReadBool('formzoom','stayontop',False);
if N21.Checked
  then FormStyle:=fsStayOnTop
  else FormStyle:=fsNormal;

Scale:=1;

Top:=FormMain.Top + 45;
Left:=FormMain.Left - 10;
end;

procedure TFormZoom.FormShow(Sender: TObject);
begin
Panel1.Width:=FormZoom.ClientWidth - 10;
Panel1.Height:=FormZoom.ClientHeight - 31;
end;

procedure TFormZoom.FormClose(Sender: TObject; var Action: TCloseAction);
begin
FormMain.Image1.Picture:=Image1.Picture;
end;

procedure TFormZoom.FormResize(Sender: TObject);
begin
Panel1.Width:=FormZoom.ClientWidth - 10;
Panel1.Height:=FormZoom.ClientHeight - 31;

if (not N2.Checked)and(not N3.Checked)and(not N7.Checked) then
  begin
  Image1.Width:=Round(Image1.Picture.Width * Scale);
  Image1.Height:=Round(Image1.Picture.Height * Scale);
  end else
  begin
  Image1.Width:=Panel1.Width;
  Image1.Height:=Panel1.Height;
  end;

if Image1.Width <= abs(Image1.Left)+Panel1.Width then
  begin
  Image1.Left:=-(Image1.Width-Panel1.Width);
  end;
if Image1.Left >= 0 then
  begin
  Image1.Left:=0;
  end;

if Image1.Height <= abs(Image1.Top)+Panel1.Height then
  begin
  Image1.Top:=-(Image1.Height-Panel1.Height);
  end;
if Image1.Top >= 0 then
  begin
  Image1.Top:=0;
  end;

StatusBar1.Top:=ClientHeight - 21;
StatusBar1.Left:=0;
StatusBar1.Width:=ClientWidth;
if N2.Checked then
  begin
  ViewScale(0);
  end;
end;

procedure TFormZoom.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
if(Key = 13)or(Key = 27)then
  Close;
end;

procedure TFormZoom.FormMouseWheelDown(Sender: TObject; Shift: TShiftState;
  MousePos: TPoint; var Handled: Boolean);
begin
FormMain.NextPicture;
end;

procedure TFormZoom.FormMouseWheelUp(Sender: TObject; Shift: TShiftState;
  MousePos: TPoint; var Handled: Boolean);
begin
FormMain.PrevPicture;
end;

// Menu

procedure TFormZoom.N2Click(Sender: TObject);
begin
N2.Checked:=True;
N14.Checked:=True;

Image1.Proportional:=True;
Image1.Stretch:=True;
SetScale(0);
ViewScale(0);
end;

procedure TFormZoom.N3Click(Sender: TObject);
begin
N3.Checked:=True;
N15.Checked:=True;
StatusBar1.Panels[1].Text:='';
Image1.Proportional:=False;
Image1.Stretch:=True;
SetScale(0);
ViewScale(-1);
end;

procedure TFormZoom.N7Click(Sender: TObject);
begin
N7.Checked:=True;
N16.Checked:=True;
Image1.Proportional:=False;
Image1.Stretch:=False;
SetScale(0);
ViewScale(100);
end;

procedure TFormZoom.N251Click(Sender: TObject);
begin
N251.Checked:=True;
N252.Checked:=True;
Image1.Proportional:=True;
Image1.Stretch:=True;
SetScale(0.25);
ViewScale(25);
end;

procedure TFormZoom.N5Click(Sender: TObject);
begin
N5.Checked:=True;
N501.Checked:=True;
Image1.Proportional:=True;
Image1.Stretch:=True;
SetScale(0.5);
ViewScale(50);
end;

procedure TFormZoom.N4Click(Sender: TObject);
begin
N4.Checked:=True;
N1001.Checked:=True;
Image1.Proportional:=True;
Image1.Stretch:=True;
SetScale(1);
ViewScale(100);
end;

procedure TFormZoom.N6Click(Sender: TObject);
begin
N6.Checked:=True;
N2001.Checked:=True;
Image1.Proportional:=True;
Image1.Stretch:=True;
SetScale(2);
ViewScale(200);
end;

procedure TFormZoom.N3001Click(Sender: TObject);
begin
N3001.Checked:=True;
N4001.Checked:=True;
Image1.Proportional:=True;
Image1.Stretch:=True;
SetScale(4);
ViewScale(400);
end;

procedure TFormZoom.N8001Click(Sender: TObject);
begin
N8001.Checked:=True;
N8002.Checked:=True;
Image1.Proportional:=True;
Image1.Stretch:=True;
SetScale(8);
ViewScale(800);
end;

// End Menu

procedure TFormZoom.Image1DblClick(Sender: TObject);
begin
if FormZoom.WindowState = wsNormal
  then FormZoom.WindowState:=wsMaximized
  else FormZoom.WindowState:=wsNormal;
end;

procedure TFormZoom.Image1MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
if (Button = mbLeft)and(not N2.Checked)and(not N3.Checked)and(not N7.Checked)then
  begin
  Timer1.Enabled:=True;
  Moving:=True;
  x0:=x;
  y0:=y;
  x1:=x;
  y1:=y;
  end else Moving:=False;
end;

procedure TFormZoom.Image1MouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
begin
if Moving then
  begin
  Screen.Cursor:=crSizeAll;
  x1:=x;
  y1:=y;
  CanRedraw:=True;
  end;
end;

procedure TFormZoom.Image1MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
Timer1.Enabled:=False;
Moving:=False;
Screen.Cursor:=crDefault;
end;

procedure TFormZoom.Timer1Timer(Sender: TObject);
begin
if CanRedraw then
  begin
  Image1.Left:=Image1.Left + x1 - x0;
  if Image1.Left <= Panel1.Width-Image1.Width then
    begin
    Image1.Left:=-(Image1.Width-Panel1.Width);
    end;
  if Image1.Left >= 0 then
    begin
    Image1.Left:=0;
    end;

  Image1.Top:=Image1.Top + y1 - y0;
  if Image1.Top <= Panel1.Height-Image1.Height then
    begin
    Image1.Top:=-(Image1.Height-Panel1.Height);
    end;
  if Image1.Top >= 0 then
    begin
    Image1.Top:=0;
    end;

  CanRedraw:=False;
  end;
end;

procedure TFormZoom.FormMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
Moving:=False;
Screen.Cursor:=crDefault;
end;

procedure TFormZoom.N10Click(Sender: TObject);
begin
FormMain.NextPicture;
end;

procedure TFormZoom.N11Click(Sender: TObject);
begin
FormMain.PrevPicture;
end;

procedure TFormZoom.N13Click(Sender: TObject);
begin
FormMain.FirstPicture;
end;

procedure TFormZoom.N12Click(Sender: TObject);
begin
FormMain.LastPicture;
end;

procedure TFormZoom.N19Click(Sender: TObject);
begin
if FormOptions.CheckBox3.Checked then
  begin
  if FormOptions.RadioButton4.Checked
    then FormMain.LastPicture
    else FormMain.FirstPicture;
  end;

FormMain.TimerShow.Interval:=FormOptions.SpinEdit1.Value * 1000;
FormMain.TimerShow.Enabled:=True;
end;

procedure TFormZoom.N20Click(Sender: TObject);
begin
FormMain.TimerShow.Enabled:=False;
end;



procedure TFormZoom.N21Click(Sender: TObject);
begin
if N21.Checked
  then FormStyle:=fsStayOnTop
  else FormStyle:=fsNormal;
end;



end.
