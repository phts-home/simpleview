object FormMain: TFormMain
  Left = 183
  Top = 200
  HorzScrollBar.Visible = False
  VertScrollBar.Visible = False
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = 'SimpleView'
  ClientHeight = 343
  ClientWidth = 706
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  Menu = MainMenu1
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  OnMouseUp = FormMouseUp
  PixelsPerInch = 96
  TextHeight = 13
  object FileListBox1: TFileListBox
    Left = 325
    Top = 35
    Width = 145
    Height = 251
    Ctl3D = True
    IntegralHeight = True
    ItemHeight = 19
    Mask = '*.jpg;*.jpeg;*.bmp;*.ico;*.tga'
    ParentCtl3D = False
    PopupMenu = PpMnShell
    TabOrder = 0
    OnClick = FileListBox1Click
    OnDblClick = FileListBox1DblClick
  end
  object FilterComboBox1: TFilterComboBox
    Left = 325
    Top = 290
    Width = 145
    Height = 21
    FileList = FileListBox1
    Filter = 
      'All Supported Filetypes|*.jpg;*.jpeg;*.bmp;*.ico;*.tga|JPEG Imag' +
      'e File (*.jpg)|*.jpg|JPEG Image File (*.jpeg)|*.jpeg|Bitmaps (*.' +
      'bmp)|*.bmp|Icons (*.ico)|*.ico|Targa (*.tga)|*.tga|All Files (*.' +
      '*)|*.*'
    TabOrder = 1
    OnChange = FilterComboBox1Change
  end
  object Edit1: TEdit
    Left = 325
    Top = 10
    Width = 145
    Height = 21
    TabOrder = 2
    OnClick = Edit1Click
    OnKeyPress = Edit1KeyPress
  end
  object ShellTreeView1: TShellTreeView
    Left = 475
    Top = 35
    Width = 216
    Height = 276
    ObjectTypes = [otFolders]
    Root = 'rfDesktop'
    ShellComboBox = ShellComboBox1
    UseShellImages = True
    AutoRefresh = False
    HideSelection = False
    Indent = 19
    ParentColor = False
    RightClickSelect = True
    ShowRoot = False
    TabOrder = 3
    OnChange = ShellTreeView1Change
  end
  object ShellComboBox1: TShellComboBox
    Left = 475
    Top = 10
    Width = 216
    Height = 22
    Root = 'rfDesktop'
    ShellTreeView = ShellTreeView1
    UseShellImages = True
    DropDownCount = 8
    TabOrder = 4
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 324
    Width = 706
    Height = 19
    Panels = <
      item
        Alignment = taCenter
        Width = 50
      end
      item
        Alignment = taCenter
        Width = 50
      end
      item
        Alignment = taCenter
        Width = 75
      end
      item
        Width = 150
      end
      item
        Width = 50
      end>
    SizeGrip = False
  end
  object Panel1: TPanel
    Left = 10
    Top = 10
    Width = 301
    Height = 301
    BevelOuter = bvNone
    PopupMenu = PopupMenu1
    TabOrder = 6
    object Image1: TImage
      Left = 0
      Top = 0
      Width = 301
      Height = 301
      Center = True
      PopupMenu = PopupMenu1
      Proportional = True
      Stretch = True
      OnDblClick = FileListBox1DblClick
      OnMouseDown = Image1MouseDown
      OnMouseMove = Image1MouseMove
      OnMouseUp = Image1MouseUp
    end
  end
  object MainMenu1: TMainMenu
    Left = 20
    Top = 20
    object N1: TMenuItem
      Caption = #1060#1072#1081#1083
      object JPEG1: TMenuItem
        Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100' '#1082#1072#1082' JPEG...'
        OnClick = JPEG1Click
      end
      object ICO1: TMenuItem
        Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100' '#1082#1072#1082' ICO...'
        OnClick = ICO1Click
      end
      object N32: TMenuItem
        Caption = '-'
      end
      object N29: TMenuItem
        Caption = #1054#1090#1082#1088#1099#1090#1100' '#1074' '#1087#1088#1086#1075#1088#1072#1084#1084#1077' '#1087#1086' '#1091#1084#1086#1083#1095#1072#1085#1080#1102
        ShortCut = 115
        OnClick = N29Click
      end
      object N30: TMenuItem
        Caption = #1059#1076#1072#1083#1080#1090#1100
        ShortCut = 46
        OnClick = N30Click
      end
      object N31: TMenuItem
        Caption = #1057#1074#1086#1081#1089#1090#1074#1072' '#1092#1072#1081#1083#1072
        OnClick = N31Click
      end
      object N22: TMenuItem
        Caption = '-'
      end
      object N4: TMenuItem
        Caption = #1042#1099#1093#1086#1076
        ShortCut = 32883
        OnClick = N4Click
      end
    end
    object N14: TMenuItem
      Caption = #1055#1088#1086#1089#1084#1086#1090#1088
      object N15: TMenuItem
        Caption = #1057#1083#1077#1076#1091#1097#1072#1103
        ShortCut = 34
        OnClick = N15Click
      end
      object N16: TMenuItem
        Caption = #1055#1088#1077#1076#1099#1076#1091#1097#1072#1103
        ShortCut = 33
        OnClick = N16Click
      end
      object N17: TMenuItem
        Caption = #1055#1077#1088#1074#1072#1103
        ShortCut = 36
        OnClick = N17Click
      end
      object N18: TMenuItem
        Caption = #1055#1086#1089#1083#1077#1076#1085#1103#1103
        ShortCut = 35
        OnClick = N18Click
      end
      object N20: TMenuItem
        Caption = '-'
      end
      object N19: TMenuItem
        Caption = #1053#1072#1095#1072#1090#1100' '#1087#1086#1082#1072#1079
        ShortCut = 120
        OnClick = N19Click
      end
      object N21: TMenuItem
        Caption = #1047#1072#1082#1086#1085#1095#1080#1090#1100' '#1087#1086#1082#1072#1079
        ShortCut = 16504
        OnClick = N21Click
      end
      object N25: TMenuItem
        Caption = '-'
      end
      object N24: TMenuItem
        Caption = #1054#1073#1085#1086#1074#1080#1090#1100
        ShortCut = 116
        OnClick = N24Click
      end
    end
    object N10: TMenuItem
      Caption = #1048#1079#1086#1073#1088#1072#1078#1077#1085#1080#1077
      object N11: TMenuItem
        AutoCheck = True
        Caption = #1055#1086#1076#1086#1075#1085#1072#1090#1100' '#1087#1086#1076' '#1088#1072#1079#1084#1077#1088
        Checked = True
        RadioItem = True
        ShortCut = 16432
        OnClick = N11Click
      end
      object N12: TMenuItem
        AutoCheck = True
        Caption = #1056#1072#1089#1090#1103#1085#1091#1090#1100
        RadioItem = True
        ShortCut = 16441
        OnClick = N12Click
      end
      object N8: TMenuItem
        AutoCheck = True
        Caption = #1055#1086' '#1094#1077#1085#1090#1088#1091
        RadioItem = True
        ShortCut = 16440
        OnClick = N8Click
      end
      object N4001: TMenuItem
        Caption = '-'
      end
      object N251: TMenuItem
        AutoCheck = True
        Caption = '25%'
        RadioItem = True
        ShortCut = 16433
        OnClick = N251Click
      end
      object N501: TMenuItem
        AutoCheck = True
        Caption = '50%'
        RadioItem = True
        ShortCut = 16434
        OnClick = N501Click
      end
      object N13: TMenuItem
        AutoCheck = True
        Caption = '100%'
        RadioItem = True
        ShortCut = 16435
        OnClick = N13Click
      end
      object N2001: TMenuItem
        AutoCheck = True
        Caption = '200%'
        RadioItem = True
        ShortCut = 16436
        OnClick = N2001Click
      end
      object N4002: TMenuItem
        AutoCheck = True
        Caption = '400%'
        RadioItem = True
        ShortCut = 16437
        OnClick = N4002Click
      end
      object N8001: TMenuItem
        AutoCheck = True
        Caption = '800%'
        RadioItem = True
        ShortCut = 16438
        OnClick = N8001Click
      end
    end
    object N2: TMenuItem
      Caption = #1055#1088#1086#1075#1088#1072#1084#1084#1072
      object N5: TMenuItem
        Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1080'...'
        ShortCut = 123
        OnClick = N5Click
      end
    end
    object N3: TMenuItem
      Caption = #1057#1087#1088#1072#1074#1082#1072
      object ReadMetxt1: TMenuItem
        Caption = 'ReadMe.txt'
        OnClick = ReadMetxt1Click
      end
      object N23: TMenuItem
        Caption = '-'
      end
      object N6: TMenuItem
        Caption = #1054' '#1087#1088#1086#1075#1088#1072#1084#1084#1077'...'
        OnClick = N6Click
      end
    end
  end
  object PopupMenu1: TPopupMenu
    Left = 50
    Top = 20
    object MenuItem1: TMenuItem
      AutoCheck = True
      Caption = #1055#1086#1076#1086#1075#1085#1072#1090#1100' '#1087#1086#1076' '#1088#1072#1079#1084#1077#1088
      Checked = True
      RadioItem = True
      ShortCut = 16432
      OnClick = N11Click
    end
    object MenuItem2: TMenuItem
      AutoCheck = True
      Caption = #1056#1072#1089#1090#1103#1085#1091#1090#1100
      RadioItem = True
      ShortCut = 16441
      OnClick = N12Click
    end
    object N9: TMenuItem
      AutoCheck = True
      Caption = #1055#1086' '#1094#1077#1085#1090#1088#1091
      RadioItem = True
      ShortCut = 16440
      OnClick = N8Click
    end
    object N7: TMenuItem
      Caption = '-'
    end
    object N252: TMenuItem
      AutoCheck = True
      Caption = '25%'
      RadioItem = True
      ShortCut = 16433
      OnClick = N251Click
    end
    object N502: TMenuItem
      AutoCheck = True
      Caption = '50%'
      RadioItem = True
      ShortCut = 16434
      OnClick = N501Click
    end
    object MenuItem3: TMenuItem
      AutoCheck = True
      Caption = '100%'
      RadioItem = True
      ShortCut = 16435
      OnClick = N13Click
    end
    object N2002: TMenuItem
      AutoCheck = True
      Caption = '200%'
      RadioItem = True
      ShortCut = 16436
      OnClick = N2001Click
    end
    object N4003: TMenuItem
      AutoCheck = True
      Caption = '400%'
      RadioItem = True
      ShortCut = 16437
      OnClick = N4002Click
    end
    object N8002: TMenuItem
      AutoCheck = True
      Caption = '800%'
      RadioItem = True
      ShortCut = 16438
      OnClick = N8001Click
    end
  end
  object TimerShow: TTimer
    Enabled = False
    OnTimer = TimerShowTimer
    Left = 80
    Top = 20
  end
  object SaveDialog1: TSaveDialog
    DefaultExt = 'bmp'
    Filter = 'Bitmaps (*.bmp)|*.bmp'
    Options = [ofOverwritePrompt, ofHideReadOnly, ofEnableSizing]
    Title = #1057#1086#1093#1088#1072#1085#1080#1090#1100' '#1082#1072#1082' BMP'
    Left = 110
    Top = 20
  end
  object SaveDialog2: TSaveDialog
    DefaultExt = 'jpg'
    Filter = 'JPEG Image File (*.jpg)|*.jpg'
    Options = [ofOverwritePrompt, ofHideReadOnly, ofEnableSizing]
    Title = #1057#1086#1093#1088#1072#1085#1080#1090#1100' '#1082#1072#1082' JPEG'
    Left = 140
    Top = 20
  end
  object SaveDialog3: TSaveDialog
    DefaultExt = 'ico'
    Filter = 'Icons (*.ico)|*.ico'
    Options = [ofOverwritePrompt, ofHideReadOnly, ofEnableSizing]
    Title = #1057#1086#1093#1088#1072#1085#1080#1090#1100' '#1082#1072#1082' ICO'
    Left = 170
    Top = 20
  end
  object TimerImage: TTimer
    Enabled = False
    Interval = 100
    OnTimer = TimerImageTimer
    Left = 200
    Top = 20
  end
  object PpMnShell: TPopupMenu
    Left = 230
    Top = 20
    object N26: TMenuItem
      Caption = #1054#1090#1082#1088#1099#1090#1100' '#1074' '#1087#1088#1086#1075#1088#1072#1084#1084#1077' '#1087#1086' '#1091#1084#1086#1083#1095#1072#1085#1080#1102
      ShortCut = 115
      OnClick = N29Click
    end
    object N28: TMenuItem
      Caption = #1059#1076#1072#1083#1080#1090#1100
      ShortCut = 46
      OnClick = N30Click
    end
    object N27: TMenuItem
      Caption = #1057#1074#1086#1081#1089#1090#1074#1072' '#1092#1072#1081#1083#1072
      OnClick = N31Click
    end
  end
end
