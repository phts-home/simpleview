program SimpleView;

uses
  Forms,
  uMain in 'uMain.pas' {FormMain},
  uZoom in 'uZoom.pas' {FormZoom},
  uAbout in 'uAbout.pas' {FormAbout},
  uOptions in 'uOptions.pas' {FormOptions},
  uDir in 'uDir.pas' {FormDir},
  uJPEGPic in 'uJPEGPic.pas' {FormJPEG};

{$R *.res}

begin
  Application.Initialize;
  Application.Title := 'SimpleView';
  Application.CreateForm(TFormMain, FormMain);
  Application.CreateForm(TFormZoom, FormZoom);
  Application.CreateForm(TFormAbout, FormAbout);
  Application.CreateForm(TFormOptions, FormOptions);
  Application.CreateForm(TFormDir, FormDir);
  Application.CreateForm(TFormJPEG, FormJPEG);
  Application.Run;
end.
